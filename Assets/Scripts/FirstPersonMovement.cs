﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonMovement : MonoBehaviour
{

    public float speed = 10.0f;
    private float translation;
    private float strafe;
    private Vector3 movement = new Vector3 (0,0,0);
    public float maxGroundDistance = 1.5f;
    private bool isGrounded;

    // Camera variables
    public Transform camera;
    public Animator cameraAnimator;
    private Vector3 height;
    public float crouchHeight = -0.1f;

    // Use this for initialization
    void Start()
    {
        height = new Vector3(0, camera.localPosition.y, 0);
    }

    void LateUpdate()
    {
        isGrounded = Physics.Raycast(transform.position, Vector3.down, maxGroundDistance);
    }

    // Update is called once per frame
    void Update()
    {
        // Input.GetAxis() is used to get the user's input
        // You can furthor set it on Unity. (Edit, Project Settings, Input)
        translation = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        strafe = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        movement.x = strafe;
        movement.z = translation;
        transform.Translate(movement);

        if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
        {
            if (isGrounded)
            {
                GetComponent<Rigidbody>().velocity = Vector3.zero;
                GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            }
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            if (camera.localPosition.y > 0)
            {
                cameraAnimator.SetBool("Crouch", true);
                camera.localPosition = new Vector3(0, crouchHeight, 0);
            }
            else
            {
                cameraAnimator.SetBool("Crouch", false);
                camera.localPosition = height;
            }
        }

        if (Input.GetKeyDown("escape"))
        {
            // turn on the cursor
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
