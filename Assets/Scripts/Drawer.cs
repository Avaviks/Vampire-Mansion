﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drawer : MonoBehaviour
{
    private Collider activate;
    public Animator drawerAnimator;
    public Transform drawerTransform;

    // Start is called before the first frame update
    void Start()
    {
        activate = GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        // When the collider is turned off
        if (activate.enabled == false)
        {
            // The drawer will open or close depending on if it is already opened or closed
            if (drawerTransform.localPosition.z < 0.1)
            {
                drawerAnimator.SetBool("IsDrawerOpen?", true);
            }
            else
            {
                drawerAnimator.SetBool("IsDrawerOpen?", false);
            }
            activate.enabled = true;
        }
    }
}
