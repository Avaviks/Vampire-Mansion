﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightSwitch : MonoBehaviour
{
    public GameObject lights;
    public GameObject switchPivot;
    private Collider activate;
    private Animator switchAnimator;

    void Start()
    {
        activate = GetComponent<Collider>();
        switchAnimator = switchPivot.GetComponent<Animator>();
        
        // Makes the switch match the state of the lights.
        if (lights.activeSelf)
        {
            switchPivot.transform.localEulerAngles = new Vector3(0, 0, 45);
        }
        else
        {
            switchPivot.transform.localEulerAngles = new Vector3(0, 0, 135);
        }
        switchAnimator.SetBool("IsLightOn?", lights.activeSelf);
    }

    void Update()
    {
        // When the collider is turned off
        if (activate.enabled == false)
        {
            // The lights will turn on or off depending on if they are already on or off
            if (lights.activeSelf)
            {
                lights.SetActive(false);
                switchPivot.transform.localEulerAngles = new Vector3(0, 0, 45);
            }
            else
            {
                lights.SetActive(true);
                switchPivot.transform.localEulerAngles = new Vector3 (0, 0, 135);
            }
            switchAnimator.SetBool("IsLightOn?", lights.activeSelf);
            activate.enabled = true;
        }
    }
}