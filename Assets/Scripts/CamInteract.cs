﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamInteract : MonoBehaviour
{
    private GameObject mainCam;
    public float distanceOfRay = 0.8f;
    public GameObject interactSymbol;


    // Start is called before the first frame update
    void Start()
    {
        mainCam = GameObject.FindWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        if (Physics.Raycast(mainCam.transform.position, mainCam.transform.forward, out var hit, distanceOfRay))
        {
            if (hit.transform.CompareTag("Interactable"))
            {
                interactSymbol.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    hit.collider.enabled = false;
                }
            }
            else
            {
                interactSymbol.SetActive(false);
            }
        }
        else
        {
            interactSymbol.SetActive(false);
        }
    }
}
