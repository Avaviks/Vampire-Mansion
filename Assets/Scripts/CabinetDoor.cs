﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CabinetDoor : MonoBehaviour
{
    private Collider activate;
    public Transform doorPivot;
    public Animator doorAnimator;

    // Start is called before the first frame update
    void Start()
    {
        activate = GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        // When the collider is turned off
        if (activate.enabled == false)
        {
            // The door will open or close depending on if it is already opened or closed
            if (doorPivot.localEulerAngles.y == 0)
            {
                doorAnimator.SetBool("IsDoorOpen?", true);
                doorPivot.localEulerAngles = new Vector3(0, 90, 0);
            }
            else
            {
                doorAnimator.SetBool("IsDoorOpen?", false);
                doorPivot.localEulerAngles = new Vector3(0, 0, 0);
            }
            activate.enabled = true;
        }
    }
}