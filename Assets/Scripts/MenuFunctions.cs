﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuFunctions : MonoBehaviour
{
    // Resets the current scene 
    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // Opens a chosen scene 
    public void OpenScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    // Opens a provided link
    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }

    // Closes the application
    public void Quit()
    {
        Application.Quit();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }
}
